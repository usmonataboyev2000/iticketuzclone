package uz.ticket.demo.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;
import uz.ticket.demo.companet.MessageService;
import uz.ticket.demo.payload.ErrorData;

import java.util.List;
@EqualsAndHashCode(callSuper = true)
@Data
public class RestException extends RuntimeException{


    private String userMsg;
    private HttpStatus status;
    private List<ErrorData> errors;

    public RestException(String userMsg, HttpStatus status) {
        super(userMsg);
        this.userMsg = userMsg;
        this.status = status;
    }

    private RestException(HttpStatus status, List<ErrorData> errors) {
        this.status = status;
        this.errors = errors;
    }

    public static RestException restThrow(String userMsg, HttpStatus httpStatus) {
        return new RestException(userMsg, httpStatus);
    }

    public static RestException restThrow(List<ErrorData> errors, HttpStatus status) {
        return new RestException(status, errors);
    }

    /**
     * @param resourceKey - {@link org.springframework.context.MessageSource} bo'yicha kelishi kerak. Masalan "ADDRESS"
     * @return Address topilmadi!
     */
    public static RestException notFound(String resourceKey) {
        return new RestException(
                MessageService.notFound(resourceKey),
                HttpStatus.NOT_FOUND
        );
    }

    public static RestException alreadyExists(String resourceKey) {
        return new RestException(
                MessageService.alreadyExists(resourceKey),
                HttpStatus.CONFLICT
        );
    }

    public static RestException attackResponse() {
        return new RestException(
                MessageService.getMessage("ATTACK_RESPONSE"),
                HttpStatus.BAD_REQUEST
        );
    }

    public static RestException forbidden() {
        return new RestException(
                MessageService.getMessage("FORBIDDEN"),
                HttpStatus.FORBIDDEN
        );
    }

    public static RestException unauthorized() {
        return new RestException(
                MessageService.getMessage("UNAUTHORIZED"),
                HttpStatus.UNAUTHORIZED
        );
    }

}
