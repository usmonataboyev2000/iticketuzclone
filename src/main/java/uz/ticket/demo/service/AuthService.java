package uz.ticket.demo.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.LoginDto;
import uz.ticket.demo.payload.SignUpDto;
import uz.ticket.demo.payload.TokenDto;

import java.util.UUID;

public interface AuthService extends UserDetailsService {

    UserDetails loadById(UUID id);

    ApiResult<TokenDto> signIn(LoginDto loginDto);

    ApiResult<TokenDto> refreshToken(TokenDto tokenDto);

    ApiResult<TokenDto> signUp(SignUpDto signUpDto);

//    ApiResult<?> checkPhoneNumber(PhoneNumberDto phoneNumberDto);

//    ApiResult<RegisterDto> checkCode(CodeDto codeDto);


}
