package uz.ticket.demo.service;

import org.springframework.stereotype.Service;
import uz.ticket.demo.payload.AddressDto;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.CustomPage;

import java.util.List;
import java.util.UUID;

@Service
public interface AddressService {
    public ApiResult<String> add(AddressDto addressDto);
    public ApiResult<String> edit(UUID id, AddressDto addressDto);

    ApiResult<List<AddressDto>> getAll();

    ApiResult<AddressDto> getById(UUID id);
}
