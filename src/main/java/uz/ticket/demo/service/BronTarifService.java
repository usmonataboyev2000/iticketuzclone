package uz.ticket.demo.service;

import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.BronTariffReqDto;
import uz.ticket.demo.payload.BronTariffResDto;

import java.util.UUID;

public interface BronTarifService {

    ApiResult<String> add(BronTariffReqDto bronTarifReqDto);

    ApiResult<BronTariffResDto> getOne(UUID id);

    ApiResult<String> delete(UUID id);
}
