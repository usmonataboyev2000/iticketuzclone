package uz.ticket.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.ticket.demo.entity.Attachment;
import uz.ticket.demo.entity.AttachmentContent;
import uz.ticket.demo.exception.RestException;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.CustomPage;
import uz.ticket.demo.repository.AttachmentContentRepository;
import uz.ticket.demo.repository.AttachmentRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AttachmentServiceImpl implements AttachmentService{

    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;
    private final CustomPage customPage;

    @Override
    public ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException {

        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());
        if (file!=null){
            String originalFilename = file.getOriginalFilename();
            Attachment attachment=new Attachment(
                    originalFilename,
                    file.getContentType(),
                    file.getSize()
            );
            Attachment save = attachmentRepository.save(attachment);
            AttachmentContent attachmentContent=new AttachmentContent(
                    file.getBytes(),
                    save
            );
            attachmentContentRepository.save(attachmentContent);
            return ApiResult.successResponse(attachment.getId());
        }
        return ApiResult.successResponse("Error");
    }

    @Override
    public ApiResult<CustomPage<Attachment>> getAll(int page, int size) {
        Pageable pageable= PageRequest.of(page,size);
        Page<Attachment> attachmentPage = attachmentRepository.findAll(pageable);
        CustomPage<Attachment> attachmentCustomPage = customPage.defaultPage(attachmentPage);
        return ApiResult.successResponse(attachmentCustomPage);
    }

    @Override
    public ApiResult<Attachment> getById(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> RestException.notFound("FILE"));
        return ApiResult.successResponse(attachment);
    }

    @Override
    public ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException {

        //FILENI BAZADAN ID ORQALI O'ZINI VA CONTENTINI TO'IQ OLISH AGAR TOPILMASA THROWGA OTAMIZ
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> RestException.notFound("FILE"));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(attachment.getId());
        if (attachmentContent==null)
            throw RestException.notFound("CONTENT");
        response.setHeader("Content-Disposition","attachment; filename=\""+attachment.getName()+"\"");
        response.setContentType(attachment.getContentType());
        FileCopyUtils.copy(attachmentContent.getBytes(),response.getOutputStream());
        return ApiResult.successResponse("Successfully");
    }
}
