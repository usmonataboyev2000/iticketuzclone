package uz.ticket.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import uz.ticket.demo.companet.MessageService;
import uz.ticket.demo.entity.Speaker;
import uz.ticket.demo.entity.Specialization;
import uz.ticket.demo.exception.RestException;
import uz.ticket.demo.mapper.SpeakerMapper;
import uz.ticket.demo.mapper.SpecializationMapper;
import uz.ticket.demo.payload.*;
import uz.ticket.demo.repository.SpeakerRepository;
import uz.ticket.demo.repository.SpecializationRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class SpeakerServiceImpl implements SpeakerService {

    private final SpeakerRepository speakerRepository;
    private final SpecializationRepository specializationRepository;
    private final SpecializationMapper specializationMapper;
    private final SpeakerMapper speakerMapper;

    @Override
    public ApiResult<String> add(SpeakerReqDto speakerReqDto) {

        //SPEKER DTONI DB GA SAQLASH UCHUN SPEKERGA O'GIRIBERYAPDI
        Speaker speaker = speakerMapper.dtoToSpeaker(speakerReqDto);

        //SPEAKERDA LAVOZIMLAR BOR BO'LSA KIRADI BU IFGA
        if (!speakerReqDto.getSpecializations().isEmpty()) {
            //todo speasialization
            HashSet<Specialization> specializations = new HashSet<>(specializationRepository.findAllById(speakerReqDto.getSpecializations().stream().map(SpecializationDto::getId).collect(Collectors.toSet())));

            //SPEKERDA LAVOZIMLAR MAVJUD BO'LSA
            if (specializations.isEmpty()) throw RestException.notFound("SPECIALIZATION");

            //SPEKERGA LAVOZIMLARNI BIRIKTIRYAPDI
            speaker.setSpecializations(specializations);
        }

        //SPEAKERNI SAQLAYAPDI
        speakerRepository.save(speaker);
        return ApiResult.successResponse(MessageService.successSave("SPEAKER"));
    }

    @Override
    public ApiResult<String> edit(Long id, SpeakerReqDto speakerReqDto) {

        //ID LI SPEAKERNI BAZADAN OLIB KELADI TOPOLMASA THROW
        Speaker speaker = speakerRepository.findById(id).orElseThrow(() -> RestException.notFound("SPEAKER "));

        //REQ DA KELGAN MA'LUMOTLARNI SPEKERGA SET QILYAPDI(MUTAHASISLIKLARINI IKKI QATOR PASTDA SET QILADI )
        speakerMapper.editSpeaker(speaker,speakerReqDto);

        Set<Specialization> editSpecializations = speakerReqDto.getSpecializations().stream().map(specializationMapper::dtoToSpecialization).collect(Collectors.toSet());
        speaker.setSpecializations(editSpecializations);

        //EDIT BO'LGAN SPEAKERNI SAQLASH
        speakerRepository.save(speaker);

        return ApiResult.successResponse(MessageService.successEdit("SPEAKER"));
    }

    @Override
    public ApiResult<SpeakerResDto> getOne(Long id) {
        //QAYTARMOQCHI BO'LGAN SPEAKERNI BAZADAN OLADI AGAR TOPOLMASA THROW
        Speaker speaker = speakerRepository.findById(id).orElseThrow(() -> RestException.notFound("SPEAKER"));

        //SPEAKERNI DTOGA O'GIRIBERADI
        SpeakerResDto speakerResDto = speakerMapper.speakerToDto(speaker);
        return ApiResult.successResponse(speakerResDto);
    }

    @Override
    public ApiResult<List<SpeakerResDto>> getAll() {
        List<Speaker> speakers = speakerRepository.findAll();
        List<SpeakerResDto> speakerResDtos = speakerMapper.speakersToDtos(speakers);

        return ApiResult.successResponse(speakerResDtos);
    }

    @Override
    public ApiResult<String> delete(Long id) {

        //O'CHIRMOQCHI BO'LGAN SPEAKERNI BAZADAN OLADI AGAR TOPOLMASA THROW
        Speaker speaker = speakerRepository.findById(id).orElseThrow(() -> RestException.notFound("SPEAKER"));

        speakerRepository.delete(speaker);
        return ApiResult.successResponse(MessageService.successDelete("SPEAKER"));
    }
}
