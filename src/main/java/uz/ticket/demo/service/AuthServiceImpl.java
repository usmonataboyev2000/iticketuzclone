package uz.ticket.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.ticket.demo.entity.User;
import uz.ticket.demo.exception.RestException;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.LoginDto;
import uz.ticket.demo.payload.SignUpDto;
import uz.ticket.demo.payload.TokenDto;
import uz.ticket.demo.repository.AttachmentRepository;
import uz.ticket.demo.repository.RoleRepository;
import uz.ticket.demo.repository.UserRepository;
import uz.ticket.demo.repository.VerificationCodeRepository;
import uz.ticket.demo.security.JWTProvider;
import uz.ticket.demo.service.base.BaseService;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class AuthServiceImpl implements AuthService {
    private final BaseService baseService;
    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JWTProvider jwtProvider;
    private final VerificationCodeRepository verificationCodeRepository;
    private final AttachmentRepository attachmentRepository;
    private final RoleRepository roleRepository;


    @Value(value = "${verificationCodeLimit}")
    private int codeLimit;
    @Value(value = "${verificationCodeTime}")
    private long codeTimeLimit;
    @Value(value = "${verificationCodeExpiredTime}")
    private Long verificationCodeExpiredTime;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(s).orElseThrow(() -> RestException.notFound("USER"));
    }

    @Override
    public ApiResult<TokenDto> signUp(SignUpDto signUpDto) {

        //FOYDALANUVCHI BAZADA OLDIN RO'YHATDAN O'TGANLIGINI TEKSHIRYAPMIZ
        boolean checkUser = userRepository.existsByPhoneNumberAndPassword(signUpDto.getPhoneNumber(), signUpDto.getPassword());

        //AGAR TRUE BO'LSA BAZADA BU PAROLLI USER MAVJUD BO'LIB BIZ XATOLIK TASHLAYMIZ
        //FALSE BO'LSA YANGI USER YASAB UNI BAZAGA SAQLAB TOKEN YASAB QAYTARAMIZ
        if (checkUser)
            throw RestException.alreadyExists("PHONE_NUMBER");
        //REGISTRATSIYA BO'LADIGAN USERNI YASAB OLYAPMIZ
        User user = baseService.dtoToUser(signUpDto);

        //REGISTRATSIYADAN MUVAFAQIYATLI O'TGAN USERNI BAZAGA SAQLAYAPMIZ
        userRepository.save(user);

        //REGISTRATSIYADAN O'TGAN USERGA ACCESS TOKEN VA REFRESH TOKEN YASAB BERADI
        TokenDto tokenDto = baseService.generateTokens(user.getId());

        // TOKENLARNI QAYTARYAPMIZ
        return ApiResult.successResponse(tokenDto);
    }

    @Override
    public UserDetails loadById(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> RestException.notFound("USER"));
    }

    @Override
    public ApiResult<TokenDto> signIn(LoginDto loginDto) {

//        try {
//            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
//                    loginDto.getUsername(),
//                    loginDto.getPassword()
//            ));
//            User user = (User) authenticate.getPrincipal();
//            TokenDto tokenDto = baseService.generateTokens(user.getId());
//            return ApiResult.successResponse(tokenDto);i_ticket_uz
//        } catch (Exception e) {
//            throw new RestException(HttpStatus.UNAUTHORIZED, "Password or username wrong!");
//        }

        Optional<User> optionalUser = userRepository.findByPhoneNumberAndPasswordAndEnabledTrue(loginDto.getUsername(), loginDto.getPassword());

        if (optionalUser.isEmpty())
            throw RestException.restThrow("ko'rsatilgan ma'lumotlar bazadagi malumotlarga to'g'ri kelmadi",HttpStatus.NOT_FOUND);

        TokenDto tokenDto = baseService.generateTokens(optionalUser.get().getId());

        return ApiResult.successResponse(tokenDto);
    }

    @Override
    public ApiResult<TokenDto> refreshToken(TokenDto tokenDto) {
        return null;
    }


}
