package uz.ticket.demo.service.base;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.ticket.demo.entity.User;
import uz.ticket.demo.exception.RestException;
import uz.ticket.demo.payload.CustomPage;
import uz.ticket.demo.payload.SignUpDto;
import uz.ticket.demo.payload.TokenDto;
import uz.ticket.demo.repository.AttachmentRepository;
import uz.ticket.demo.repository.RoleRepository;
import uz.ticket.demo.repository.UserRepository;
import uz.ticket.demo.security.JWTProvider;
import uz.ticket.demo.utils.AppConstant;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AttachmentRepository attachmentRepository;
    private final JWTProvider jwtProvider;

    //registratsiyadan o'tayotgan accauntga user yasaberadi
    public User dtoToUser(SignUpDto signUpDto){
        return new User(
                signUpDto.getFirstName(),
                signUpDto.getLastName(),
                signUpDto.getPhoneNumber(),
                signUpDto.getPassword(),
                roleRepository.findByName(AppConstant.USER).orElseThrow(() -> RestException.notFound("ROLE")),
                signUpDto.getAttachmentId() != null?attachmentRepository.findById(signUpDto.getAttachmentId()).orElseThrow(() ->RestException.notFound("AVATAR")):null,
                true
        );
    }

    public TokenDto generateTokens (UUID userId){

        String accessToken = jwtProvider.generateTokenFromId(userId, true);
        String refreshToken = jwtProvider.generateTokenFromId(userId, false);
        return new TokenDto(accessToken,refreshToken);
    }

//
//    public static CustomPage<T> page(Page<T> tPage){
//
//    }
}
