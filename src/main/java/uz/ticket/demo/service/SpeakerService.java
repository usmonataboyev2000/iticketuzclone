package uz.ticket.demo.service;

import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.SpeakerReqDto;
import uz.ticket.demo.payload.SpeakerResDto;

import java.util.List;

public interface SpeakerService {


    ApiResult<String> add(SpeakerReqDto speakerReqDto);

    ApiResult<String> edit(Long id, SpeakerReqDto speakerReqDto);

    ApiResult<SpeakerResDto> getOne(Long id);

    ApiResult<List<SpeakerResDto>> getAll();

    ApiResult<String> delete(Long id);
}
