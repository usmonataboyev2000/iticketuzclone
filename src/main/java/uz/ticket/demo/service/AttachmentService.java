package uz.ticket.demo.service;

import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.ticket.demo.entity.Attachment;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.CustomPage;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public interface AttachmentService {
    ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException;

    ApiResult<CustomPage<Attachment>> getAll(int page, int size);

    ApiResult<Attachment> getById(UUID id);

    ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException;
}
