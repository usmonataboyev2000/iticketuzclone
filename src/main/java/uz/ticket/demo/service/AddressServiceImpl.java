package uz.ticket.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.ticket.demo.companet.MessageService;
import uz.ticket.demo.entity.Address;
import uz.ticket.demo.exception.RestException;
import uz.ticket.demo.mapper.AddressMapper;
import uz.ticket.demo.payload.AddressDto;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.CustomPage;
import uz.ticket.demo.repository.AddressRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;
    private final CustomPage customPage;

    @Override
    public ApiResult<String> add(AddressDto addressDto) {

        //ADDRESS NAME UNIQUE BO'LGANI UCHUN  NAME ORQALI BU ADDRES OLDIN BAZAGA QO'SHILGANLIGINI TEKSHIRAMIZ,BOR BO'LSA THROW
        boolean check = addressRepository.existsByName(addressDto.getName());
        if (check) throw RestException.alreadyExists("ADDRESS");

        //DTO DA KELGAN MA'LUMOTLAR ORQALI YANGI ADRES YASABERYAPDI
        Address address = addressMapper.dtoToAddress(addressDto);

        //YANGI ADRESNI BAZAGA SAQLAB QO'YYAPMIZ
        addressRepository.save(address);

        return ApiResult.successResponse(MessageService.successSave("ADDRESS"));
    }

    @Override
    public ApiResult<String> edit(UUID id, AddressDto addressDto) {

        //O'ZGARTIRMOQCHI BO'LGAN ADRESIMIZNI BAZADAN IZLAYMIZ TOPILMASA THROW
        Address address = addressRepository.findById(id)
                .orElseThrow(() -> RestException.notFound("ADDRESS"));

        //YANGI ADRESIMIZ NAME NI BAZADA YO'QLIGINI TEKSHIRAMIZ TOPILSA THROW
        boolean change = addressRepository.existsByNameAndIdNot(addressDto.getName(), id);
        if (change) throw RestException.alreadyExists("ADDRESS");

        //EDIT QILINMOQCHI BO'LGAN ADDRESSGA DTODA KELGANLARNI O'ZLASHTIRIBERADI
        addressMapper.updateWithOutId(address, addressDto);

        //ADRESDAGI O'ZGARISHLARNI BAZAGA SAQLAYMIZ
        addressRepository.save(address);

        return ApiResult.successResponse(MessageService.successEdit("ADDRESS"));
    }

    @Override
    public ApiResult<List<AddressDto>> getAll() {

        return ApiResult.successResponse(

                //BAZADAN BARCHA ADDRESSLARNI OLYAPMIZ
                addressRepository.findAll()

                        //OLINGAN ADDRESSLARNI ADDRESSDTO GA O'GIRYAPMIZ
                        .stream().map(addressMapper::addressToDto)

                        //VA DTOLARNI LISTGA O'RAYAPMIZ
                        .collect(Collectors.toList())
        );
    }

    @Override
    public ApiResult<AddressDto> getById(UUID id) {

        //ID ORQALI ADRESNI BAZADAN IZLAYDI TOPOLMASA THROW
        Address address = addressRepository.findById(id).orElseThrow(() -> RestException.notFound("ADDRESS"));

        //BAZADAAN OLINGAN ADRESNI DTOGA O'RAB QAYTARADI
        AddressDto addressDto = addressMapper.addressToDto(address);
        return ApiResult.successResponse(addressDto);
    }
}
