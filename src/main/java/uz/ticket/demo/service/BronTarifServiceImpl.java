package uz.ticket.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.ticket.demo.companet.MessageService;
import uz.ticket.demo.entity.BronTariff;
import uz.ticket.demo.exception.RestException;
import uz.ticket.demo.mapper.BronTarifMapper;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.BronTariffReqDto;
import uz.ticket.demo.payload.BronTariffResDto;
import uz.ticket.demo.repository.BronRepository;
import uz.ticket.demo.repository.BronTariffRepository;

import java.sql.Timestamp;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BronTarifServiceImpl implements BronTarifService {

    private final BronTarifMapper bronTarifMapper;
    private final BronTariffRepository bronTariffRepository;
    private final BronRepository bronRepository;
    @Override
    public ApiResult<String> add(BronTariffReqDto bronTariffReqDto) {

        //DTO DAGILAR ORQALI YANGI BRONTARIF YASABERADI
        BronTariff bronTariff = bronTarifMapper.dtoToBronTariff(bronTariffReqDto);

        //BRONTARIFF NING TUGASH VAQTINI SET QILIB QO'YYAPMIZ
        long expireTime = (long) (System.currentTimeMillis() + bronTariffReqDto.getFinishTime() * 1000 * 3600);
        bronTariff.setExpireTime(new Timestamp(expireTime));

        //BRONTARIFF NI BAZAGA SAQLAYMIZ
        bronTariffRepository.save(bronTariff);
        return ApiResult.successResponse(MessageService.successSave("BRON_TARIFF"));

    }

    @Override
    public ApiResult<BronTariffResDto> getOne(UUID id) {

        //BRONTARIFF BAZADAN OLISH TOPILMASA THROW
        BronTariff bron_tariff = bronTariffRepository.findById(id).orElseThrow(() -> RestException.notFound("BRON_TARIFF"));

        //BAZADAN OLINGAN BRONTARIFF NI RESDTOGA O'GIRIB QAYTARAPYAPDI
        BronTariffResDto bronTariffResDto = bronTarifMapper.bronTariffToResDto(bron_tariff);
        return ApiResult.successResponse(bronTariffResDto);
    }

    @Override
    public ApiResult<String> delete(UUID id) {
        //todo bunda chala joyi bor
        bronTariffRepository.deleteById(id);
        return ApiResult.successResponse(MessageService.successDelete("BRON_TARIFF"));
    }
}
