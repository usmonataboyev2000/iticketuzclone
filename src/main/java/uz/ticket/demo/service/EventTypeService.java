package uz.ticket.demo.service;

import org.springframework.stereotype.Service;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.EventTypeDto;
import uz.ticket.demo.payload.EventTypeResDto;

import java.util.List;


public interface EventTypeService {

    ApiResult<String> add(EventTypeDto eventTypeDto);

    ApiResult<String> edit(Long id, EventTypeDto eventTypeDto);

    ApiResult<EventTypeResDto> getOne(Long id);

    ApiResult<List<EventTypeResDto>> getAll();

    ApiResult<String> delete(Long id);
}
