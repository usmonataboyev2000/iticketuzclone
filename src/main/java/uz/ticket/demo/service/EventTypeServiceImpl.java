package uz.ticket.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.ticket.demo.companet.MessageService;
import uz.ticket.demo.entity.EventType;
import uz.ticket.demo.exception.RestException;
import uz.ticket.demo.mapper.EventTypeMapper;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.EventTypeDto;
import uz.ticket.demo.payload.EventTypeResDto;
import uz.ticket.demo.repository.EventTypeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EventTypeServiceImpl implements EventTypeService{

    private final EventTypeRepository eventTypeRepository;
    private final EventTypeMapper eventTypeMapper;


    @Override
    public ApiResult<String> add(EventTypeDto eventTypeDto) {
        boolean existsByName = eventTypeRepository.existsByName(eventTypeDto.getName());
        if (existsByName)throw RestException.alreadyExists("EVENT_TYPE");
        EventType eventType = eventTypeMapper.dtoToEventType(eventTypeDto);
        eventTypeRepository.save(eventType);
        return ApiResult.successResponse(MessageService.successSave("EVENT_TYPE"));
    }

    @Override
    public ApiResult<String> edit(Long id, EventTypeDto eventTypeDto) {

        EventType eventType = eventTypeRepository.findById(id).orElseThrow(() -> RestException.notFound("EVENT TYPE"));

        boolean existsByName = eventTypeRepository.existsByName(eventTypeDto.getName());
        if (existsByName)throw RestException.alreadyExists("EVENT TYPE ");

        eventTypeMapper.updateEventType(eventType,eventTypeDto);

        eventTypeRepository.save(eventType);
        return ApiResult.successResponse(MessageService.successEdit("EVENT TYPE"));
    }

    @Override
    public ApiResult<EventTypeResDto> getOne(Long id) {

        EventType eventType = eventTypeRepository.findById(id).orElseThrow(() -> RestException.notFound("EVENT TYPE"));

        EventTypeResDto eventTypeResDto = eventTypeMapper.eventTypeToDto(eventType);

        return ApiResult.successResponse(eventTypeResDto);
    }

    @Override
    public ApiResult<List<EventTypeResDto>> getAll() {

        List<EventType> all = eventTypeRepository.findAll();
        List<EventTypeResDto> collect = all.stream().map(eventTypeMapper::eventTypeToDto).collect(Collectors.toList());
        return ApiResult.successResponse(collect);
    }

    @Override
    public ApiResult<String> delete(Long id) {

        EventType eventType = eventTypeRepository.findById(id).orElseThrow(() -> RestException.notFound("EVENT TYPE"));

        eventTypeRepository.delete(eventType);
        return ApiResult.successResponse(MessageService.successDelete("EVENT_TYPE"));
    }
}
