package uz.ticket.demo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import uz.ticket.demo.entity.EventType;
import uz.ticket.demo.payload.EventTypeDto;
import uz.ticket.demo.payload.EventTypeResDto;

@Mapper(componentModel = "spring")
public interface EventTypeMapper {

    @Mapping(target = "id",ignore = true)
    EventType dtoToEventType(EventTypeDto eventTypeDto);

    @Mapping(target = "id",ignore = true)
    void updateEventType(@MappingTarget EventType eventType, EventTypeDto eventTypeDto);


    EventTypeResDto eventTypeToDto(EventType eventType);
}
