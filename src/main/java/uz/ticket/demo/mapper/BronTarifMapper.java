package uz.ticket.demo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import uz.ticket.demo.entity.Address;
import uz.ticket.demo.entity.BronTariff;
import uz.ticket.demo.payload.AddressDto;
import uz.ticket.demo.payload.BronTariffReqDto;
import uz.ticket.demo.payload.BronTariffResDto;

@Mapper(componentModel = "spring")
public interface BronTarifMapper {

    /**
     * {@link BronTariffReqDto} ni {@link BronTariff} ga o'giriberadi
     */
    BronTariff dtoToBronTariff(BronTariffReqDto bronTariffReqDto);

    /**
     * {@link BronTariff} ni {@link BronTariffResDto} ga o'giriberadi
     */
    BronTariffResDto bronTariffToResDto(BronTariff bron_tariff);
}
