package uz.ticket.demo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import uz.ticket.demo.entity.Address;
import uz.ticket.demo.payload.AddressDto;
import uz.ticket.demo.utils.MappingUtils;



@Mapper(componentModel = "spring")
public interface AddressMapper {

    /**
     * {@link AddressDto} ni {@link Address} ga o'giriberadi
     */
     Address dtoToAddress(AddressDto addressDto);

    @Mapping(target = "id", ignore = true)
    void updateWithOutId(@MappingTarget Address address, AddressDto addressDto);

    AddressDto addressToDto(Address address);
}
