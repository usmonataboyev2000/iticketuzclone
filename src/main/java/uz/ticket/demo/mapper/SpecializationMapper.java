package uz.ticket.demo.mapper;

import org.mapstruct.Mapper;
import uz.ticket.demo.entity.Specialization;
import uz.ticket.demo.payload.SpecializationByIdDto;
import uz.ticket.demo.payload.SpecializationDto;

@Mapper(componentModel = "spring")
public interface SpecializationMapper {


   Specialization dtoToSpecialization(SpecializationDto specializationDto);
   Specialization dtoToSpecializationById(Long id);

   SpecializationDto specializationToDto(Specialization specialization);
}
