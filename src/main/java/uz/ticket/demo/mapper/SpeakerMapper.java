package uz.ticket.demo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.ticket.demo.entity.Speaker;
import uz.ticket.demo.payload.SpeakerReqDto;
import uz.ticket.demo.payload.SpeakerResDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SpeakerMapper {

    @Mapping(target = "specializations", ignore = true)
    Speaker dtoToSpeaker(SpeakerReqDto speakerReqDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "specializations", ignore = true)
    void editSpeaker(@MappingTarget Speaker speaker, SpeakerReqDto newSpeaker);


    SpeakerResDto speakerToDto (Speaker speaker);

    List<SpeakerResDto> speakersToDtos(List<Speaker> speakers);
}
