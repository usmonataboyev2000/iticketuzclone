package uz.ticket.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.demo.entity.Person;

import java.util.Optional;
import java.util.UUID;

public interface PersonRepository extends JpaRepository<Person, UUID> {
    Optional<Person> findByPhoneNumber(String phoneNumber);
}
