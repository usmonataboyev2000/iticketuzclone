package uz.ticket.demo.repository;

import uz.ticket.demo.entity.EventSpeaker;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface EventSpeakerRepository extends JpaRepository<EventSpeaker, UUID> {
}
