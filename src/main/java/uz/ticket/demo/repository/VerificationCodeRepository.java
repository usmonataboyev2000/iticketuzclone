package uz.ticket.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.demo.entity.VerificationCode;

import java.util.UUID;

public interface VerificationCodeRepository extends JpaRepository<VerificationCode, UUID> {

boolean existsByPhoneNumberAndCodeAndConfirmedTrue(String phoneNumber, String code);
}
