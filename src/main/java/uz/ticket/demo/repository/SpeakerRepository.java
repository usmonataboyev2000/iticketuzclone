package uz.ticket.demo.repository;

import uz.ticket.demo.entity.Speaker;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SpeakerRepository extends JpaRepository<Speaker, Long> {
}
