package uz.ticket.demo.repository;

import uz.ticket.demo.entity.Specialization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;


public interface SpecializationRepository extends JpaRepository<Specialization, Long> {
    boolean existsByName(String name);

    boolean existsByNameAndId(String name, Long id);
}
