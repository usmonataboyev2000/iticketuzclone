package uz.ticket.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.demo.entity.Bron;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BronRepository extends JpaRepository<Bron, UUID> {

}
