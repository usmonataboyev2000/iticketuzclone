package uz.ticket.demo.repository;

import uz.ticket.demo.entity.EventType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface EventTypeRepository extends JpaRepository<EventType, Long> {
    boolean existsByName(String name);
}
