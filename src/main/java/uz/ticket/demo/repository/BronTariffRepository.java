package uz.ticket.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.demo.entity.BronTariff;

import java.util.UUID;

public interface BronTariffRepository extends JpaRepository<BronTariff, UUID> {
}
