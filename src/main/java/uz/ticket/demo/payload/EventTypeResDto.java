package uz.ticket.demo.payload;

import lombok.Data;

@Data
public class EventTypeResDto {

    private Long id;
    private String name;
}
