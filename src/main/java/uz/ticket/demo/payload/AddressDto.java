package uz.ticket.demo.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;


@Data@AllArgsConstructor@NoArgsConstructor
public class  AddressDto {

    private double lat;

    private double lon;

    private String name;

    private String target;

    private UUID photoId;


}
