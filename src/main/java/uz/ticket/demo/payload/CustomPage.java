package uz.ticket.demo.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Component
public class CustomPage<T> {

    private Collection<T> content; // Elementlar
    private int numberOfElements;  // Current page dagi elementlar soni
    private int number;            // Current page number
    private long totalElements;    // Barcha elementlar soni
    private int totalPages;        // Barcha page lar soni
    private int size;              // Nechta so'ragani

    public CustomPage<T> defaultPage(Page<T> tPage) {
        return new CustomPage<>(
                tPage.getContent(),
                tPage.getNumberOfElements(),
                tPage.getNumber(),
                tPage.getTotalElements(),
                tPage.getTotalPages(),
                tPage.getSize()
        );
    }
}
