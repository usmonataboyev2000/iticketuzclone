package uz.ticket.demo.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@AllArgsConstructor@NoArgsConstructor@Data
public class SpeakerResDto {

    private Long id;

    private String fullName;

    private Set<SpecializationDto> specializations;

    private String description;

    private UUID photoId;
}
