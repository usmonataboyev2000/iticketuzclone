package uz.ticket.demo.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.ticket.demo.entity.Attachment;
import uz.ticket.demo.entity.Specialization;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor@NoArgsConstructor@Data
public class SpeakerReqDto {

    private String fullName;

    private Set<SpecializationDto> specializations;

    private String description;

    private UUID photoId;
}
