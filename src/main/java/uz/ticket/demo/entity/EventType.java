package uz.ticket.demo.entity;

import uz.ticket.demo.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.ticket.demo.entity.template.AbsLongEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SQLDelete(sql = "update event_type set deleted=true where id=?")
@Where(clause = "deleted=false")
public class EventType extends AbsLongEntity {

    @Column(nullable = false, unique = true)
    private String name;

}
