package uz.ticket.demo.entity;

import uz.ticket.demo.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.ticket.demo.entity.template.AbsLongEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update event_speaker set deleted=true where id=?")
@Where(clause = "deleted=false")
public class EventSpeaker extends AbsLongEntity {

    @ManyToOne
    private Event event;

    @ManyToOne
    private Speaker speaker;

    private boolean main;
}
