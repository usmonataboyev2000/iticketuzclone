package uz.ticket.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.ticket.demo.entity.template.AbsEntity;
import uz.ticket.demo.entity.template.AbsLongEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update address set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Address extends AbsEntity {

    private double lat;

    private double lon;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String target;

    @OneToOne
    @JoinColumn(name = "photo_id", insertable = false, updatable = false)
    private Attachment photo;

    @Column(name = "photo_id")
    private UUID photoId;

}
