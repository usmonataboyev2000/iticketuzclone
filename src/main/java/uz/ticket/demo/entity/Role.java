package uz.ticket.demo.entity;

import uz.ticket.demo.entity.template.AbsEntity;
import uz.ticket.demo.entity.template.AbsLongEntity;
import uz.ticket.demo.enums.PermissionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update role set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Role extends AbsEntity {

    @Column(unique = true)
    private String name;

    @Column(columnDefinition = "text")
    private String description;

    @Enumerated(EnumType.STRING)
    @ElementCollection
    private Set<PermissionEnum> permissions;
}
