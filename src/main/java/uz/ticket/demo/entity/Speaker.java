package uz.ticket.demo.entity;

import uz.ticket.demo.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.ticket.demo.entity.template.AbsLongEntity;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update speaker set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Speaker extends AbsLongEntity {

    private String fullName;

    @JoinColumn(name ="speaker_specialization")
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Specialization> specializations;

    @Column(columnDefinition = "text")
    private String description;


    @OneToOne(fetch = FetchType.LAZY)
    private Attachment photo;

}
