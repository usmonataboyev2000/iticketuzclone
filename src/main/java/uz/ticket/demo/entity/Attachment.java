package uz.ticket.demo.entity;

import uz.ticket.demo.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.ticket.demo.entity.template.AbsLongEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update attachment set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Attachment extends AbsEntity {

    private String name;

    private String contentType;

    private long size;

}
