package uz.ticket.demo.companet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.ticket.demo.entity.EventType;
import uz.ticket.demo.entity.Role;
import uz.ticket.demo.entity.User;
import uz.ticket.demo.enums.PermissionEnum;
import uz.ticket.demo.repository.EventTypeRepository;
import uz.ticket.demo.repository.RoleRepository;
import uz.ticket.demo.repository.UserRepository;
import uz.ticket.demo.utils.AppConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final EventTypeRepository eventTypeRepository;

    public DataLoader(RoleRepository roleRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, EventTypeRepository eventTypeRepository) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.eventTypeRepository = eventTypeRepository;
    }

    @Value("${dataLoaderMode}")
    private String dataLoaderMode;


    @Override
    public void run(String... args) throws Exception {

        if (dataLoaderMode.equals("always")) {
            Role admin = roleRepository.save(new Role(
                    AppConstant.ADMIN,
                    "bu admin",
                    new HashSet<>(Arrays.asList(PermissionEnum.values()))));

            roleRepository.save(new Role(
                    AppConstant.USER,
                    "bu client",
                    new HashSet<>()));

            userRepository.save(new User(
                    "admin",
                    "adminov",
                    "+998995096123",
                    passwordEncoder.encode("admin123"),
                    admin,
                    null,
                    true));

            ArrayList<EventType> eventTypes =new ArrayList<>();
            eventTypes.add(new EventType("KAMEDIYA"));
            eventTypes.add(new EventType("DRAMA"));
            eventTypes.add(new EventType("TRILLER"));
            eventTypes.add(new EventType("ROMANTIC"));
            eventTypes.add(new EventType("TEATR"));
            eventTypes.add(new EventType("SIRK"));
            eventTypes.add(new EventType("ARALASH"));
            eventTypeRepository.saveAll(eventTypes);
        }
    }
}
