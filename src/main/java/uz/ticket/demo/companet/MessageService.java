package uz.ticket.demo.companet;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import uz.ticket.demo.payload.ErrorData;

import java.util.ArrayList;


@Component
@RequiredArgsConstructor
public class MessageService {


private static MessageSource messageSource;


    public static String getMessage(String key) {
        return key;
    }

    private static String merge(String action, String sourceKey) {
        return String.format(action+"%s", sourceKey);
    }

    //========================SUCCESS======================

    public static String successSave(String sourceKey) {
        return merge("SUCCESS_SAVE_", sourceKey);
    }

    public static String successEdit(String sourceKey) {
        return merge("SUCCESS_EDIT_", sourceKey);
    }

    public static String successDelete(String sourceKey) {
        return merge("SUCCESS_DELETE_", sourceKey);
    }

    //=======================ERROR=======================

    public static String cannotDelete(String sourceKey)
    {
        return merge("CANNOT_DELETE_", sourceKey);
    }

    public static String notFound(String sourceKey) {

        return merge("NOT_FOUND_", sourceKey);
    }

    public static String alreadyExists(String sourceKey)
    {
        return merge("ALREADY_EXISTS_", sourceKey);
    }


}
