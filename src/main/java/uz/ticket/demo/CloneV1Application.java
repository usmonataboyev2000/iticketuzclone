package uz.ticket.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@ComponentScan({"uz.ticket.demo.companet"})
@EntityScan("uz.ticket.demo.entity,uz.ticket.demo.payload.CustomPage, uz.ticket.demo.config,uz.ticket.demo.mapper")
@EnableJpaRepositories("uz.ticket.demo.repository")
public class CloneV1Application {

    public static void main(String[] args) {
        SpringApplication.run(CloneV1Application.class, args);
    }

}
