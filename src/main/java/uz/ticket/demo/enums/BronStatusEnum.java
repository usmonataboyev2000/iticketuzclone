package uz.ticket.demo.enums;

public enum BronStatusEnum {
    ACTIVE,
    COMPLETE,
    CANCEL
}
