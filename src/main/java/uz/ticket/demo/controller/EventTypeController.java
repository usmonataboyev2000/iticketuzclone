package uz.ticket.demo.controller;

import org.springframework.web.bind.annotation.*;
import uz.ticket.demo.entity.EventType;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.EventTypeDto;
import uz.ticket.demo.payload.EventTypeResDto;
import uz.ticket.demo.utils.RestConstant;

import java.util.List;

@RequestMapping(path = RestConstant.EVENT_TYPE_CONTROLLER)
public interface EventTypeController {

    @PostMapping("/add")
    ApiResult<String>add(@RequestBody EventTypeDto eventTypeDto);

    @PutMapping("/edit/{id}")
    ApiResult<String>edit(@PathVariable Long id,@RequestBody EventTypeDto eventTypeDto);

    @GetMapping("/getOne/{id}")
    ApiResult<EventTypeResDto>getOne(@PathVariable Long id);

    @GetMapping("/getAll")
    ApiResult<List<EventTypeResDto>>getAll();

    @DeleteMapping("/delete/{id}")
    ApiResult<String>delete(@PathVariable Long id);
}

