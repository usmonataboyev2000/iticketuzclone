package uz.ticket.demo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.BronTariffReqDto;
import uz.ticket.demo.payload.BronTariffResDto;
import uz.ticket.demo.service.BronTarifService;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class BronTarifControllerImpl implements BronTarifController {

    private final BronTarifService bronTarifService;

    @Override
    public ApiResult<String> add(BronTariffReqDto bronTarifReqDto) {
        log.info("add metod entered: {}", bronTarifReqDto);
        ApiResult<String> result = bronTarifService.add(bronTarifReqDto);
        log.info("add metod finished: {}", result);
        return result;
    }

    @Override
    public ApiResult<BronTariffResDto> getOne(UUID id) {
        log.info("getOne metod entered: {}",id);
        ApiResult<BronTariffResDto> result = bronTarifService.getOne(id);
        log.info("getOne method finished: {}",result);
        return result;
    }

    @Override
    public ApiResult<String> delete(UUID id) {
        log.info("delete method entered: {}",id);
        ApiResult<String> result = bronTarifService.delete(id);
        log.info("delete method finished: {}",result);
        return result;
    }
}
