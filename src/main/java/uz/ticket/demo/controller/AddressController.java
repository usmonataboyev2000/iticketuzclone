package uz.ticket.demo.controller;

import org.springframework.web.bind.annotation.*;
import uz.ticket.demo.entity.Address;
import uz.ticket.demo.payload.AddressDto;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.CustomPage;
import uz.ticket.demo.utils.AppConstant;
import uz.ticket.demo.utils.RestConstant;

import java.util.List;
import java.util.UUID;

@RequestMapping(path = RestConstant.ADDRESS_CONTROLLER)
@RestController
public interface AddressController {

    @PostMapping("/add")
    ApiResult<String> add(@RequestBody AddressDto addressDto);

    @PostMapping("/edit/{id}")
    ApiResult<String> edit(@PathVariable UUID id, @RequestBody AddressDto addressDto);

    @GetMapping("getAll")
    ApiResult<List<AddressDto>>getAll();


    @GetMapping("get/{id}")
    ApiResult<AddressDto>getById(@PathVariable UUID id);


}
