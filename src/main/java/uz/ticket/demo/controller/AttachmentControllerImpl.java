package uz.ticket.demo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.ticket.demo.entity.Attachment;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.CustomPage;
import uz.ticket.demo.service.AttachmentService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AttachmentControllerImpl implements AttachmentController {

    private final AttachmentService attachmentService;

    @Override
    public ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException {
        return attachmentService.upload(request);
    }

    @Override
    public ApiResult<CustomPage<Attachment>> getAll(int page, int size) {
        return attachmentService.getAll(page, size);
    }

    @Override
    public ApiResult<Attachment> getId(UUID id) {
        return attachmentService.getById(id);
    }

    @Override
    public ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException {
        return attachmentService.getFile(id, response);
    }
}
