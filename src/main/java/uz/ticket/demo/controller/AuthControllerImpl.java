package uz.ticket.demo.controller;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;
import uz.ticket.demo.payload.*;
import uz.ticket.demo.service.AuthService;

import javax.validation.Valid;
@Service
@RequiredArgsConstructor
public class AuthControllerImpl implements AuthController {
    private final AuthService authService;
    @Override
    public ApiResult<TokenDto> signIn(@Valid LoginDto loginDto) {
        return authService.signIn(loginDto);
    }

    @Override
    public ApiResult<TokenDto> signUp(@Valid SignUpDto signUpDto) {
        return authService.signUp(signUpDto);
    }

    @Override
    public ApiResult<?> checkPhoneNumber(@Valid PhoneNumberDto phoneNumberDto) {
        return null;
    }

    @Override
    public ApiResult<RegisterDto> checkCode(@Valid CodeDto codeDto) {
        return null;
    }

    @Override
    public ApiResult<TokenDto> refreshToken(TokenDto tokenDto) {
        return authService.refreshToken(tokenDto);
    }
}
