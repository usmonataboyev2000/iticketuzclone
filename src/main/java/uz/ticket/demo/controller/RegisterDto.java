package uz.ticket.demo.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor@NoArgsConstructor@Data
public class RegisterDto {

    private String accessToken;
    private String refreshToken;
    private  boolean registered;

}
