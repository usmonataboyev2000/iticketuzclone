package uz.ticket.demo.controller;

import org.springframework.web.bind.annotation.*;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.BronTariffReqDto;
import uz.ticket.demo.payload.BronTariffResDto;
import uz.ticket.demo.utils.RestConstant;

import java.util.UUID;

@RequestMapping(path = RestConstant.BRON_TARIFF_CONTROLLER)
public interface BronTarifController {

    @PostMapping("/add")
    ApiResult<String>add(@RequestBody BronTariffReqDto bronTarifReqDto);

    @GetMapping("/get/{id}")
    ApiResult<BronTariffResDto>getOne(@PathVariable UUID id);

    @DeleteMapping("/delete/{id}")
    ApiResult<String>delete(@PathVariable UUID id);

}
