package uz.ticket.demo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.demo.companet.MessageService;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.EventTypeDto;
import uz.ticket.demo.payload.EventTypeResDto;
import uz.ticket.demo.repository.EventTypeRepository;
import uz.ticket.demo.service.EventTypeService;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class EventTypeControllerImpl implements EventTypeController{
    @Autowired
    private EventTypeService eventTypeService;

    @Override
    public ApiResult<String> add(EventTypeDto eventTypeDto) {
        log.info("event type add start: {}",eventTypeDto);
        ApiResult<String> result = eventTypeService.add(eventTypeDto);
        log.info("event type add finish: {}",result);
        return result;
    }

    @Override
    public ApiResult<String> edit(Long id, EventTypeDto eventTypeDto) {
        log.info("event type edit start: id = {}, {} ",id,eventTypeDto);
        ApiResult<String> result = eventTypeService.edit(id,eventTypeDto);
        log.info("event type edit finish: {}",result);
        return result;
    }

    @Override
    public ApiResult<EventTypeResDto> getOne(Long id) {
        log.info("event type getOne entered : id = {} ",id);
        ApiResult<EventTypeResDto> result = eventTypeService.getOne(id);
        log.info("event type getOne finished : {}",result);
        return result;
    }

    @Override
    public ApiResult<List<EventTypeResDto>> getAll() {
        log.info("event type getAll method entered:");
        ApiResult<List<EventTypeResDto>> result = eventTypeService.getAll();
        log.info("event type getAll method finished: {}",result);
        return result;
    }

    @Override
    public ApiResult<String> delete(Long id) {
        log.info("event type delete method entered : id = {} ",id);
        ApiResult<String> result = eventTypeService.delete(id);
        log.info("event type delete method finished : {}",result);
        return result;
    }
}
