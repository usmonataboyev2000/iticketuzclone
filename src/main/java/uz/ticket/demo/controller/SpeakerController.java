package uz.ticket.demo.controller;

import org.springframework.web.bind.annotation.*;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.SpeakerReqDto;
import uz.ticket.demo.payload.SpeakerResDto;
import uz.ticket.demo.utils.RestConstant;

import java.util.List;

@RequestMapping(path = RestConstant.SPEAKER_CONTROLLER)
public interface SpeakerController {

    @PostMapping("/add")
    ApiResult<String> add(@RequestBody SpeakerReqDto speakerReqDto);

    @PostMapping("/edit/{id}")
    ApiResult<String> edit(@PathVariable Long id,@RequestBody SpeakerReqDto speakerReqDto);

    @GetMapping("getOne/{id}")
    ApiResult<SpeakerResDto> getOne(@PathVariable Long id);

    @GetMapping("/getAll")
    ApiResult<List<SpeakerResDto>> getAll();

    @DeleteMapping("/delete/{id}")
    ApiResult<String>delete(@PathVariable Long id);
}
