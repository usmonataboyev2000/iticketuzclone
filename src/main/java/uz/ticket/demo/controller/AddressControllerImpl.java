package uz.ticket.demo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.demo.payload.AddressDto;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.CustomPage;
import uz.ticket.demo.service.AddressService;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AddressControllerImpl implements AddressController {
    private final AddressService addressService;

    @Override
    public ApiResult<String> add(AddressDto addressDto) {

        log.info("add method entered: {}", addressDto);
        ApiResult<String> result = addressService.add(addressDto);
        log.info("edit method exited: {} ", result);
        return result;
    }

    @Override
    public ApiResult<String> edit(UUID id, AddressDto addressDto) {

        log.info("edit method entered id: {}, {}", id, addressDto);
        ApiResult<String> result = addressService.edit(id, addressDto);
        log.info("edit method exited: {} ", result);
        return result;
    }

    @Override
    public ApiResult<List<AddressDto>> getAll() {
        log.info("getAll method entered" );
        return addressService.getAll();
    }


    @Override
    public ApiResult<AddressDto> getById(UUID id) {
        return addressService.getById(id);
    }
}
