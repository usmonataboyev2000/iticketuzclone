package uz.ticket.demo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.SpeakerReqDto;
import uz.ticket.demo.payload.SpeakerResDto;
import uz.ticket.demo.service.SpeakerService;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class SpeakerControllerImpl implements SpeakerController {

    private final SpeakerService speakerService;

    @Override
    public ApiResult<String> add(SpeakerReqDto speakerReqDto) {
        log.info("add method entered: {}", speakerReqDto);
        ApiResult<String> result = speakerService.add(speakerReqDto);
        log.info("add method finished: {}", result);
        return result;
    }

    @Override
    public ApiResult<String> edit(Long id, SpeakerReqDto speakerReqDto) {
        log.info("edit  method entered: {}, {}", id, speakerReqDto);
        ApiResult<String> result = speakerService.edit(id, speakerReqDto);
        log.info("edit method finished: {}", result);
        return result;

    }

    @Override
    public ApiResult<SpeakerResDto> getOne(Long id) {
        log.info("getOne  method entered: {}", id);
        ApiResult<SpeakerResDto> result = speakerService.getOne(id);
        log.info("edit method finished: {}", result);
        return result;
    }

    @Override
    public ApiResult<List<SpeakerResDto>> getAll() {
        log.info("getAll  method entered");
        ApiResult<List<SpeakerResDto>> result = speakerService.getAll();
        log.info("getAll  method finished: {}",result);
        return result;
    }

    @Override
    public ApiResult<String> delete(Long id) {
        log.info("delete  method entered: {}", id);
        ApiResult<String> result = speakerService.delete(id);
        log.info("delete method finished: {}", result);
        return result;
    }


}
