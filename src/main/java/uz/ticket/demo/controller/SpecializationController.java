package uz.ticket.demo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import uz.ticket.demo.companet.MessageService;
import uz.ticket.demo.entity.Specialization;
import uz.ticket.demo.exception.RestException;
import uz.ticket.demo.mapper.SpecializationMapper;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.SpecializationDto;
import uz.ticket.demo.repository.SpecializationRepository;
import uz.ticket.demo.utils.RestConstant;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = RestConstant.SPECIALIZATION_CONTROLLER)
public class SpecializationController {

    /**
     * <h2>KICHGINA CONTROLLER BO'LGANI UCHUN SHU CLASSNI O'ZIDA YOZILDI LOGIKA KUSURA ETIBOR BERMANGIZ</h2>
     */
    private final SpecializationRepository specializationRepository;
    private final SpecializationMapper specializationMapper;

    @PostMapping("/add")
    ApiResult<String> add(@RequestBody SpecializationDto specializationDto) {
        log.info("add method entered: {}", specializationDto.getName());
        //MUTAHASISLIK BAZADA BOR BO'LSA THROWGA OTADI
        boolean change = specializationRepository.existsByName(specializationDto.getName());
        if (change) throw RestException.alreadyExists("Specialization");

        //BAZAGA SAQLAYAPDI MUTAHASISLIKNI
        specializationRepository.save(new Specialization(specializationDto.getName()));
        log.info("add method finished");
        return ApiResult.successResponse(MessageService.successSave("SPECIALIZATION"));
    }

    @GetMapping("/getOne/{id}")
    ApiResult<String> getOne(@PathVariable Long id) {
        Specialization specialization = specializationRepository.findById(id).orElseThrow(() -> RestException.notFound("SPECIALIZATION"));
        return ApiResult.successResponse(specialization.getName());
    }

    @GetMapping("/getAll")
    ApiResult<List<SpecializationDto>> getAll() {

        //BAZADAN BARCHA SPECIALIZATIONLARNI OLIB ISMLARINI LISTGA O'RAB QAYTARYAPDI
        List<SpecializationDto> specializationList = specializationRepository.findAll().stream().map(specialization -> specializationMapper.specializationToDto(specialization)).collect(Collectors.toList());

        return ApiResult.successResponse(specializationList);
    }


    @DeleteMapping("/delete/{id}")
    ApiResult<String> delete(@PathVariable Long id) {
        try {
            specializationRepository.deleteById(id);
            return ApiResult.successResponse(MessageService.successDelete("SPECIALIZATION"));
        } catch (Exception e) {
            throw RestException.notFound("SPECIALIZATION");
        }
    }
}
