package uz.ticket.demo.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.ticket.demo.entity.Attachment;
import uz.ticket.demo.payload.ApiResult;
import uz.ticket.demo.payload.CustomPage;
import uz.ticket.demo.utils.AppConstant;
import uz.ticket.demo.utils.RestConstant;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping(RestConstant.ATTACHMENT_CONTROLLER)
public interface AttachmentController {

    //FAYL(RASM)NI TIZIMGA YUKLASH
    @PostMapping("/upload")
    ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException;

    //RASMLAR MALUMOTLARINI SAHIFALAB OLISH
    @GetMapping("/info")
    ApiResult<CustomPage<Attachment>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                             @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    //ID SI ORQALI RASMNING MALUMOTINI OLISH
    @GetMapping("/info/{id}")
    ApiResult<Attachment> getId(@PathVariable UUID id);

    //FAYL(RASM)NI YUKLASH
    @GetMapping("download/{id}")
    ApiResult<?> getFile(@PathVariable UUID id, HttpServletResponse response) throws IOException;

}
